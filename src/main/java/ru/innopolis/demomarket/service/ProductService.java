package ru.innopolis.demomarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.innopolis.demomarket.dto.ProductDto;
import ru.innopolis.demomarket.entity.Product;
import ru.innopolis.demomarket.repository.ProductRepository;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProductService {

    private final ProductRepository productRepository;
    private final String imageDir;

    @Autowired
    public ProductService(ProductRepository productRepository,
                          @Value("${upload.dir.location}")String imageDir) {
        this.productRepository = productRepository;
        this.imageDir = imageDir;
    }

    public void save(ProductDto productDto) throws IOException {
        Optional<MultipartFile> image = Optional.ofNullable(productDto.getImg());
        String imgName = null;
        if (image.isPresent()){
            imgName = image.get().getOriginalFilename();
            Path imgFullPath = Paths.get(imageDir).resolve(imgName);
            Files.write(imgFullPath, image.get().getBytes());
        }

        Product product = Product.builder()
                .name(productDto.getName())
                .price(new BigDecimal(productDto.getPrice()))
                .isActive(isActive(productDto))
                .dtCreated(LocalDateTime.now())
                .imageSource(imgName)
                .build();

        productRepository.save(product);
    }

    private boolean isActive(ProductDto productDto) {
        return productDto.getIsActive() != null
                ? productDto.getIsActive()
                : false;
    }

    @Transactional(readOnly = true)
    public List<ProductDto> findAllasDTO(){
        return asDto(productRepository.findAll());
    }

    @Transactional(readOnly = true)
    public List<ProductDto> findAllIsActiveAsDTO(){
        return asDto(productRepository.findByIsActive(true));
    }

    private List<ProductDto> asDto(List<Product> products){
        return products.stream().map(this::toDto).collect(Collectors.toList());
    }

    private ProductDto toDto(Product product) {
        return ProductDto.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice().toString())
                .dtCreated(product.getDtCreated())
                .isActive(product.getIsActive())
                .imageSource(product.getImageSource())
                .build();
    }

    public ProductDto findAsDTO(long id) {
        return toDto(productRepository.findById(id).orElseThrow());
    }

    public void update(ProductDto productDto) {
        Product product = productRepository
                .findById(productDto.getId())
                .orElseThrow();
        product.setName(productDto.getName());
        product.setPrice(new BigDecimal(productDto.getPrice()));
        product.setIsActive(isActive(productDto));

        productRepository.save(product);
    }
}
