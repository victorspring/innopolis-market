package ru.innopolis.demomarket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.innopolis.demomarket.dto.ProductDto;
import ru.innopolis.demomarket.service.ProductService;

import java.io.IOException;

@Controller
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @GetMapping
    public String products(ModelMap modelMap){
        modelMap.put("products", productService.findAllIsActiveAsDTO());
        return "index";
    }

    @GetMapping("/product/add")
    public String productAdd(){
        return "product-add";
    }

    @PostMapping("/product/add")
    public String productAdd(ProductDto productDto) throws IOException {
        productService.save(productDto);
        return "redirect:/product/add";
    }

    @GetMapping("/product/{id}/update")
    public String productUpdate(@PathVariable("id") long id, ModelMap modelMap){
        modelMap.put("product", productService.findAsDTO(id));
        return "product-update";
    }

    @PostMapping("/product/update")
    public String productUpdate(@ModelAttribute("product") ProductDto productDto){
        productService.update(productDto);
        return "redirect:/";
    }




}
