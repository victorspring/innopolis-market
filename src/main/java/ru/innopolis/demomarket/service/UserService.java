package ru.innopolis.demomarket.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.innopolis.demomarket.dto.UserDto;

public interface UserService extends UserDetailsService {
    void register(UserDto userDto);
}
