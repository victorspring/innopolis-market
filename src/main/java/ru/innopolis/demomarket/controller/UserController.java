package ru.innopolis.demomarket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.innopolis.demomarket.dto.ProductDto;
import ru.innopolis.demomarket.dto.UserDto;
import ru.innopolis.demomarket.service.UserService;

import java.io.IOException;

@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String register(){
        return "register";
    }

    @PostMapping("/register")
    public String register(UserDto userDto) throws IOException {
        userService.register(userDto);
        return "redirect:/product/add";
    }
}
