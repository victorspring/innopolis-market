package ru.innopolis.demomarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.innopolis.demomarket.dto.UserDto;
import ru.innopolis.demomarket.entity.Role;
import ru.innopolis.demomarket.entity.User;
import ru.innopolis.demomarket.repository.RoleRepository;
import ru.innopolis.demomarket.repository.UserRepository;


@Service
@Transactional
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder encoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           RoleRepository roleRepository,
                           BCryptPasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow();
    }

    @Override
    public void register(UserDto userDto) {
        User user = User.builder()
                .username(userDto.getUsername())
                .password(encoder.encode(userDto.getPassword()))
                .role(getUseRole(userDto))
                .build();

        userRepository.save(user);

    }

    private Role getUseRole(UserDto userDto){
        boolean isAdmin = userDto.getIsAdmin() != null
                ? userDto.getIsAdmin()
                : false;

        return isAdmin
                ? roleRepository.findByName(RoleRepository.ROLE_ADMIN).orElseThrow()
                : roleRepository.findByName(RoleRepository.ROLE_USER).orElseThrow();


    }
}
