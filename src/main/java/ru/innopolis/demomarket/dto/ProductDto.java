package ru.innopolis.demomarket.dto;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDto {
    private Long id;
    private String name;
    private String price;
    private Boolean isActive;
    private LocalDateTime dtCreated;
    private MultipartFile img;
    private String imageSource;
}
