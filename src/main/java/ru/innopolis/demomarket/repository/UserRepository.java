package ru.innopolis.demomarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.innopolis.demomarket.entity.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);
}
