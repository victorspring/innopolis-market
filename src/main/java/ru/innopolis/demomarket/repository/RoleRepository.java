package ru.innopolis.demomarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.innopolis.demomarket.entity.Role;
import ru.innopolis.demomarket.entity.User;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {

    String ROLE_ADMIN = "ADMIN";
    String ROLE_USER = "USER";

    Optional<Role> findByName(String name);
}
