drop table if exists order_product;
drop table if exists "order";
drop table if exists product;
drop table if exists "user";
drop table if exists role;

create table role(
    id bigint primary key,
    name varchar(150) unique not null
);

create table "user"(
    id bigserial primary key,
    username varchar(150) unique not null,
    password varchar(150) not null,
    role_id bigint references role(id)
);

create table product(
    id bigserial primary key,
    name varchar(150) not null,
    price decimal(12,2) not null,
    dt_created timestamp default now(),
    is_active boolean,
    image_source text
);

create table "order"(
    id bigserial primary key,
    dt_created timestamp default now(),
    user_id bigint references "user"(id)
);


create table order_product(
    order_id bigint references "order"(id) on delete cascade,
    product_id bigint references product(id) on delete cascade,
    count int,
    primary key (order_id, product_id)
);

insert into role(id, name) values(1, 'ADMIN');
insert into role(id, name) values(2, 'USER');

select * from role;
select * from "user";
