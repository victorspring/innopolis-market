package ru.innopolis.demomarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.innopolis.demomarket.entity.Product;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findByName(String name);

    List<Product> findByIsActive(Boolean isActive);
}
