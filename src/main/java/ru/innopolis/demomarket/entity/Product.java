package ru.innopolis.demomarket.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private BigDecimal price;

    @Column(name = "dt_created")
    private LocalDateTime dtCreated;

    @Column(name = "is_active")
    private Boolean isActive;

    @ManyToMany(mappedBy = "products")
    private List<Order> orders;

    @Column(name = "image_source")
    private String imageSource;

}
